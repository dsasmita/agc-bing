<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use Storage;
use File;
use Intervention\Image\ImageManagerStatic as ImgConverter;

use App\Helpers\Buchin\Bing\Image;
use App\Helpers\Buchin\Bing\Web;
use App\Helpers\Buchin\Bing\Video;

use App\Models\post;
use App\Models\failedLog;

class homeController extends Controller
{
    public function index(Request $request){
    	return view('admin.home');
    }

    public function post(Request $request){
    	$arr_data = [];
    	$filter   = $request->input();
    	$arr_data['filter'] = $filter;

    	$posts = post::where(function ($query) use ($filter){
                if(@$filter['source'] != ''){
                	$query->where('source', $filter['source']);
                }

                if(@$filter['grower'] != ''){
                	$query->where('grower', $filter['grower']);
                }
            });

        if(@$filter['order_by'] == '')
            $posts = $posts->orderBy('created_at','desc')->paginate(10);
        else if(@$filter['order_by'] == 'slug')
            $posts = $posts->orderBy('slug','desc')->paginate(10);

        $arr_data['posts'] = $posts;

    	return view('admin.post', $arr_data);
    }

    public function processAgc(Request $request){
    	$input = $request->all();

        $rules = [
        			'date'    => 'required|date_format:Y-m-d',
                    'filecsv' => 'required|mimes:csv,txt'
                ];
        $validator = Validator::make($input
                        , $rules
        );

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)->withInput();
        }

        $filecsv = $request->file('filecsv');

        if (!is_null($filecsv) && $filecsv->getError() == 0) {
            $destinationPath = 'uploads' . DS . 'tmp' . DS ;
            $extension = $filecsv->getClientOriginalExtension();
            $fileName = 'file-csv.' . $extension;
            $filecsv->move($destinationPath, $fileName);

            $csvFile = $destinationPath . $fileName;
            $areas = $this->csv_to_array($csvFile,'|');
            
            // dd($areas);

            ini_set('memory_limit', '512M');
            ini_set('max_execution_time', '1800');

            foreach ($areas as $key => $data) {
            	$post = [];
            	$post['title'] = '';
            	$post['date'] = $request->input('date');
            	$post['slug'] = '';
            	$post['slug_category'] = $request->input('slug_category');
            	$post['content'] = '';

            	$post['path_image'] = '';
            	$post['link_image'] = '';
            	$post['link_video'] = '';
            	$post['link_doc']   = '';
            	$post['many_image'] = '';

            	$post['seo_title'] = '';
            	$post['seo_keyword'] = '';
            	$post['seo_description'] = '';
            	$post['source'] = 'seed';

            	// parsing data

            	$post['title'] = $data[0];
            	$post['slug']  = str_slug($data[0]);

            	$postData = post::where('slug', $post['slug'])->first();
            	$checkLogs = failedLog::where('description', $post['title'])->first();

				if(!$postData && !$checkLogs){
					$webScraper = new Web;
					$contents = $webScraper->scrape('"'.$data[0].'"');

					$content = '';
	            	foreach ($contents as $key => $value) {
						$content = $content. ' '.$value['description'];
					}

                    if($content == ''){
                        $contents = $webScraper->scrape($data[0]);

                        $content = '';
                        foreach ($contents as $key => $value) {
                            $content = $content. ' '.$value['description'];
                        }
                    }

					$post['content'] =  str_replace('...', '', $content);

					$post['seo_title'] = $data[0];
	            	$post['seo_keyword'] =  str_replace(';', ',', @$data[1]);
	            	$post['seo_description'] = $content;

					$imageScraper = new Image;
					$images = $imageScraper->scrape('"'.$data[0].'"','', ['image_size' => 'large' ]);

					if(count($images) > 0){
						$post['link_image'] = $images[0]['mediaurl'];
					}else{
						$imageScraper = new Image;
						$images = $imageScraper->scrape('"'.$data[0].'"','', ['image_size' => 'medium' ]);

						if(count($images) > 0){
							$post['link_image'] = $images[0]['mediaurl'];
						}
					}

					if($post['link_image'] != '' && $this->checkRemoteFile($post['link_image'])  && @getimagesize($post['link_image'])){
						$path = $post['link_image'];
						$checkExt = pathinfo(basename($path), PATHINFO_EXTENSION);

						$ext = '';
						if (strpos($checkExt, 'png') !== false) {
						    $ext = 'png';
						}
						if (strpos($checkExt, 'gif') !== false) {
						    $ext = 'png';
						}
						if (strpos($checkExt, 'jpg') !== false) {
						    $ext = 'jpg';
						}
						if (strpos($checkExt, 'jpeg') !== false) {
						    $ext = 'jpeg';
						}

						$checkImgLink = post::where('link_image', $post['link_image'])->first();

						if($ext != '' && !$checkImgLink){
							$filename = $post['title'].'.'.$ext;
							$filenameSquare = $post['title'].'_square'.'.'.$ext;
							$filename300 = $post['title'].'_300px'.'.'.$ext;
							$filename700 = $post['title'].'_700px'.'.'.$ext;

							$base_url = 'uploads/'.date('Y/m/d/',strtotime($post['date']));
				            $exist = file_exists($base_url);
				            if(!$exist){
				                mkdir($base_url, 0777, true);
				            }

				            try {
				            	ImgConverter::make($path)->save(public_path($base_url.$filename));
								// 300px
								ImgConverter::make($base_url.$filename)->widen(300)->save(public_path($base_url.$filename300));
								// 700px
								ImgConverter::make($base_url.$filename)->widen(700)->save(public_path($base_url.$filename700));
								// square
								ImgConverter::make($base_url.$filename)->widen(400)->save(public_path($base_url.$filenameSquare));
								ImgConverter::make($base_url.$filenameSquare)->resizeCanvas(400,400)->save(public_path($base_url.$filenameSquare));

								$post['path_image'] = $base_url.$filename;
				            } catch (Exception $e) {
				            	
				            }
			        	}						
					}

					// $videoScraper = new Video;
					// $video = $videoScraper->scrape('"'.$data[0].'"');

					// if(count($video) > 0){
					// 	$post['link_video'] = $video[0]['link'];
					// }else{
					// 	$videoScraper = new Video;
					// 	$video = $videoScraper->scrape($data[0]);

					// 	if(count($video) > 0){
					// 		$post['link_video'] = $video[0]['link'];
					// 	}
					// }

					if($post['path_image'] !== ''){
                        echo $data[0].' - inserted<br>';
						$postData = post::create($post);
					}else{
                        echo $data[0].' - failed to insert<br>';
						$failed = failedLog::create(
            				['description' => $post['title']]
            			);
					}
				}else{
                    echo $data[0].' - failed to insert<br>';
                }
            }
        }
        return 'done';
    }

    public function checkRemoteFile($url)
	{
		try {
			$ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL,$url);
		    // don't download content
		    curl_setopt($ch, CURLOPT_NOBODY, 1);
		    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    if(curl_exec($ch)!==FALSE){
		    	return true;
		    }else{
		    	return false;
		    }
		} catch (Exception $e) {
			return false;
		}
	}

    public function delete($id){
        $post = post::where('id',$id)->first();
        if(!$post)
            return 'not listed or done listed';

        $post->delete();

        return 'Post success deleted <a href="'.route('admin.post').'">Back</a>';
    }

	public function getGrower($id){
		$post = post::where('id',$id)->where('source','seed')->where('grower','not-yet')->first();
		if(!$post)
			return 'not listed or done listed';

		if($post->seo_keyword == '')
			return 'keyword empty';

		$keyword = explode(',', $post->seo_keyword);
		
		ini_set('memory_limit', '512M');
        ini_set('max_execution_time', '1800');

		foreach ($keyword as $key => $li) {
			$result = $this->generateContentSingle($li, $post);
			echo $li.' - '.$result.'<br>';
		}

		$post->grower = 'done';
		$post->save();

		dd('finished');
	}

	public function generateContentSingle($title, $p){
		$post = [];
        $post['title'] = '';
        $post['date'] = $p->date;
        $post['slug'] = '';

        $slug = str_slug($title);

        $post['slug_category'] = $p->slug_category;

        $post['content'] = '';

        $post['path_image'] = '';
        $post['link_image'] = '';
        $post['link_video'] = '';
        $post['link_doc']   = '';
        $post['many_image'] = '';
        $post['source'] = 'seed-related';

        $post['seo_title'] = '';
        $post['seo_keyword'] = '';
        $post['seo_description'] = '';

        // parsing data
        $post['title'] = $title;
        $post['slug']  = $slug;

        $postData = post::where('slug', $post['slug'])->first();
        $checkLogs = failedLog::where('description', $post['title'])->first();

        if(!$postData && !$checkLogs){
            $webScraper = new Web;
            $contents = $webScraper->scrape('"'.$post['title'].'"');

            $content = '';
            foreach ($contents as $key => $value) {
                $content = $content. ' '.$value['description'];
            }
            $post['content'] =  str_replace('...', '', $content);

            $post['seo_title'] = $post['title'];
            $post['seo_keyword'] =  $p->seo_keyword;
            $post['seo_description'] = $content;

            $imageScraper = new Image;
            $images = $imageScraper->scrape('"'.$post['title'].'"','', ['image_size' => 'large' ]);

            if(count($images) > 0){
                $post['link_image'] = $images[0]['mediaurl'];
            }else{
                $imageScraper = new Image;
                $images = $imageScraper->scrape($post['title']);

                if(count($images) > 0){
                    $post['link_image'] = $images[0]['mediaurl'];
                }
            }

            if($post['link_image'] != '' && $this->checkRemoteFile($post['link_image']) && @getimagesize($post['link_image']) ){
                $path = $post['link_image'];
                $checkExt = pathinfo(basename($path), PATHINFO_EXTENSION);

                $ext = '';
                if (strpos($checkExt, 'png') !== false) {
                    $ext = 'png';
                }
                if (strpos($checkExt, 'gif') !== false) {
                    $ext = 'png';
                }
                if (strpos($checkExt, 'jpg') !== false) {
                    $ext = 'jpg';
                }
                if (strpos($checkExt, 'jpeg') !== false) {
                    $ext = 'jpeg';
                }

				$checkImgLink = post::where('link_image', $post['link_image'])->first();

                if($ext != '' && !$checkImgLink){
                    $filename = $post['title'].'.'.$ext;
                    $filenameSquare = $post['title'].'_square'.'.'.$ext;
                    $filename300 = $post['title'].'_300px'.'.'.$ext;
                    $filename700 = $post['title'].'_700px'.'.'.$ext;

                    $base_url = 'uploads/'.date('Y/m/d/',strtotime($post['date']));
                    $exist = file_exists($base_url);
                    if(!$exist){
                        mkdir($base_url, 0777, true);
                    }

                    try {
                        ImgConverter::make($path)->save(public_path($base_url.$filename));
                        // 300px
                        ImgConverter::make($base_url.$filename)->widen(300)->save(public_path($base_url.$filename300));
                        // 700px
                        ImgConverter::make($base_url.$filename)->widen(700)->save(public_path($base_url.$filename700));
                        // square
                        ImgConverter::make($base_url.$filename)->widen(400)->save(public_path($base_url.$filenameSquare));
                        ImgConverter::make($base_url.$filenameSquare)->resizeCanvas(400,400)->save(public_path($base_url.$filenameSquare));

                        $post['path_image'] = $base_url.$filename;
                    } catch (Exception $e) {
                        
                    }
                }                       
            }

            // $videoScraper = new Video;
            // $video = $videoScraper->scrape('"'.$post['title'].'"');

            // if(count($video) > 0){
            //     $post['link_video'] = $video[0]['link'];
            // }else{
            //     $videoScraper = new Video;
            //     $video = $videoScraper->scrape($post['title']);

            //     if(count($video) > 0){
            //         $post['link_video'] = $video[0]['link'];
            //     }
            // }
            
            $postData = [];
            if($post['path_image'] !== ''){
	            $postData = post::create($post);
            	return 'inserted';
            }else{
            	$failed = failedLog::create(
            				['description' => $post['title']]
            			);
            	return 'failed to insert';
            }
        }

        return 'exits';
    }

    public function csv_to_array($filename='', $delimiter=','){
		if(!file_exists($filename) || !is_readable($filename))
		    return FALSE;

		$header = NULL;
		$data = array();
		if (($handle = fopen($filename, 'r')) !== FALSE)
		{
		    while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
		    {
		        if(!$header){
		            $tmp = array();
		            $i = 0;
		            foreach ($row as $key => $value) {
		                $tmp[] = $i++;
		            }
		            $header = $tmp;
		        }
		        else{
		            $tmp = array();
		            foreach ($row as $key => $value) {
		                $tmp[] = trim($value);
		            }
		            if(count($tmp) == count($header))
		                $data[] = array_combine($header, $tmp);
		        }
		    }
		    fclose($handle);
		}
		return $data;
	}

    public function onePieceChar(Request $request){
        return 'char';
    }
}