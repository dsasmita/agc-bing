<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Models\post;

use Storage;
use File;
use Intervention\Image\ImageManagerStatic as ImgConverter;

use App\Helpers\Buchin\Bing\Image;
use App\Helpers\Buchin\Bing\Web;
use App\Helpers\Buchin\Bing\Video;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $arr_data = [];
        $arr_data['seo']['title']       = env('SEO_TITLE');
        $arr_data['seo']['keywords']    = env('SEO_KEYWORDS');
        $arr_data['seo']['description'] = env('SEO_DESCRIPTION');
        $arr_data['seo']['url']         = $request->url();
        $arr_data['seo']['image']       = asset('assets/images/home.jpeg');

        $arr_data['title'] = env('SEO_TITLE');
        $arr_data['lists'] = post::orderBy('date','desc')->orderBy('created_at','desc')->paginate(10);

        return view('front.home', $arr_data);
    }

    public function detail($slug ,Request $request){
        $post = post::where('slug',$slug)->first();
        if(!$post){
            \Session::flash('notif-error', 'Page not found');
            return redirect(route('front.home'));
        }
        $arr_data = [];
        $arr_data['seo']['title']       = $post->title.' | '.env('SEO_SITE_NAME');
        $arr_data['seo']['keywords']    = $post->seo_keyword;
        $arr_data['seo']['description'] = $post->content;
        $arr_data['seo']['url']         = $request->url();
        $arr_data['seo']['image']       = url($post->path_image);

        $arr_data['title'] = $post->title .' | '.env('SEO_SITE_NAME');
        $arr_data['post']  = $post;
        $arr_data['lists'] = post::orderBy('date','desc')->limit(15)->get();

        return view('front.detail', $arr_data);
    }

    public function category($slug, Request $request)
    {
        if($slug == ''){
            \Session::flash('notif-error', 'Page not found');
            return redirect(route('front.home'));
        }
        $arr_data = [];
        $arr_data['seo']['title']       = env('SEO_TITLE');
        $arr_data['seo']['keywords']    = env('SEO_KEYWORDS');
        $arr_data['seo']['description'] = env('SEO_DESCRIPTION');
        $arr_data['seo']['url']         = $request->url();
        $arr_data['seo']['image']       = asset('assets/images/home.jpeg');

        $arr_data['title'] = $slug.' | '.env('SEO_SITE_NAME');
        $arr_data['lists'] = post::where('slug_category', $slug)->orderBy('date','desc')->orderBy('created_at','desc')->paginate(15);

        return view('front.home', $arr_data);
    }

    public function pageRedirect($page, Request $request){
        return redirect(route('front.home').'?page='.$page);
    }

    public function generateContent($slug){
        $post = [];
        $post['title'] = '';
        $post['date'] = date('Y-m-d');
        $post['slug'] = '';

        if(strpos($slug, 'deck') !== false) {
            $post['slug_category'] = 'deck';
        }else if(strpos($slug, 'rose') !== false){
            $post['slug_category'] = 'rose';
        }else if(strpos($slug, 'patio') !== false){
            $post['slug_category'] = 'patio';
        }else{
            $post['slug_category'] = 'uncategorized';
        }

        $post['content'] = '';

        $post['path_image'] = '';
        $post['link_image'] = '';
        $post['link_video'] = '';
        $post['link_doc']   = '';
        $post['many_image'] = '';

        $post['seo_title'] = '';
        $post['seo_keyword'] = '';
        $post['seo_description'] = '';

        // parsing data
        $post['title'] = str_replace('-', ' ', $slug);
        $post['slug']  = $slug;

        $postData = post::where('slug', $post['slug'])->first();
        if(!$postData){
            $webScraper = new Web;
            $contents = $webScraper->scrape('"'.$post['title'].'"');

            $content = '';
            foreach ($contents as $key => $value) {
                $content = $content. ' '.$value['description'];
            }
            $post['content'] =  str_replace('...', '', $content);

            $post['seo_title'] = $post['title'];
            $post['seo_keyword'] =  str_replace(';', ',', @$data[1]);
            $post['seo_description'] = $content;

            $imageScraper = new Image;
            $images = $imageScraper->scrape('"'.$post['title'].'"','', ['image_size' => 'large' ]);

            if(count($images) > 0){
                $post['link_image'] = $images[0]['mediaurl'];
            }else{
                $imageScraper = new Image;
                $images = $imageScraper->scrape($post['title']);

                if(count($images) > 0){
                    $post['link_image'] = $images[0]['mediaurl'];
                }
            }

            if($post['link_image'] != '' && $this->checkRemoteFile($post['link_image'])){
                $path = $post['link_image'];
                $checkExt = pathinfo(basename($path), PATHINFO_EXTENSION);

                $ext = '';
                if (strpos($checkExt, 'png') !== false) {
                    $ext = 'png';
                }
                if (strpos($checkExt, 'gif') !== false) {
                    $ext = 'png';
                }
                if (strpos($checkExt, 'jpg') !== false) {
                    $ext = 'jpg';
                }
                if (strpos($checkExt, 'jpeg') !== false) {
                    $ext = 'jpeg';
                }

                if($ext != ''){
                    $filename = $post['title'].'.'.$ext;
                    $filenameSquare = $post['title'].'_square'.'.'.$ext;
                    $filename300 = $post['title'].'_300px'.'.'.$ext;
                    $filename700 = $post['title'].'_700px'.'.'.$ext;

                    $base_url = 'uploads/'.date('Y/m/d/',strtotime($post['date']));
                    $exist = file_exists($base_url);
                    if(!$exist){
                        mkdir($base_url, 0777, true);
                    }

                    try {
                        ImgConverter::make($path)->save(public_path($base_url.$filename));
                        // 300px
                        ImgConverter::make($base_url.$filename)->widen(300)->save(public_path($base_url.$filename300));
                        // 700px
                        ImgConverter::make($base_url.$filename)->widen(700)->save(public_path($base_url.$filename700));
                        // square
                        ImgConverter::make($base_url.$filename)->widen(400)->save(public_path($base_url.$filenameSquare));
                        ImgConverter::make($base_url.$filenameSquare)->resizeCanvas(400,400)->save(public_path($base_url.$filenameSquare));

                        $post['path_image'] = $base_url.$filename;
                    } catch (Exception $e) {
                        
                    }
                }                       
            }

            $videoScraper = new Video;
            $video = $videoScraper->scrape('"'.$post['title'].'"');

            if(count($video) > 0){
                $post['link_video'] = $video[0]['link'];
            }else{
                $videoScraper = new Video;
                $video = $videoScraper->scrape($post['title']);

                if(count($video) > 0){
                    $post['link_video'] = $video[0]['link'];
                }
            }
            
            $postData = [];
            if($post['path_image'] !== '')
                $postData = post::create($post);

            return $postData;
        }
    }

    public function checkRemoteFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        // don't download content
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(curl_exec($ch)!==FALSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function sitemapXml(Request $request){
        $sitemap = \App::make("sitemap");

        // if(env('APP_ENV') == 'production')
        //     $sitemap->setCache('laravel.sitemap-alamat', 3600);

        $products = post::orderBy('created_at','asc')->paginate(2500);
        foreach ($products as $prod)
        {
            $sitemap->add(route('front.detail', $prod->slug), $prod->updated_at, '0.9', 'daily');
        }
        return $sitemap->render('xml');
    }
}
