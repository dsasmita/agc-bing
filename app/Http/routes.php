<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['namespace' => 'Admin\Auth'], function(){
	Route::get('/login',['as' => 'admin.login', 'uses' => 'AuthController@showLoginForm']);
	Route::post('/login',['as' => 'admin.login.post', 'uses' => 'AuthController@login']);
	Route::get('/logout',['as' => 'admin.logout', 'uses' => 'AuthController@logout']);
});


Route::group(['middleware' => ['auth'],'namespace' => 'Admin', 'prefix' => 'admin'], function(){
    Route::get('/', ['as' => 'admin.home', 'uses' => 'homeController@index']);
    Route::post('/process-agc', ['as' => 'admin.agc.process', 'uses' => 'homeController@processAgc']);

    Route::get('/post', ['as' => 'admin.post', 'uses' => 'homeController@post']);
    Route::get('/post/grower-{id}', ['as' => 'admin.post.grower', 'uses' => 'homeController@getGrower']);
    
    Route::get('/post/delete-{id}', ['as' => 'admin.post.delete', 'uses' => 'homeController@delete']);

    Route::get('/one-piece-char', ['as' => 'admin.onepiece.char', 'uses' => 'homeController@onePieceChar']);
});

Route::get('/', ['as' => 'front.home', 'uses' => 'HomeController@index']);
Route::get('sitemap.xml', ['as' => 'front.sitemap.xml', 'uses' => 'HomeController@sitemapXml']);
Route::get('/category/{slug}', ['as' => 'front.category', 'uses' => 'HomeController@category']);
Route::get('/page/{id}', ['as' => 'front.page', 'uses' => 'HomeController@pageRedirect'])->where(['id' => '[0-9]+']);
Route::get('/{slug}', ['as' => 'front.detail', 'uses' => 'HomeController@detail']);