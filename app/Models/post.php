<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class post extends Model
{
	protected $table = 'posts';
	protected $fillable = [
				'source',
				'grower',
				'title',
				'date' ,
				'slug',
				'slug_category',
				'content',
				'path_image',
				'link_image',
				'link_video',
				'link_doc',
				'many_image',
				'seo_title',
				'seo_keyword',
				'seo_description',
	];
}
