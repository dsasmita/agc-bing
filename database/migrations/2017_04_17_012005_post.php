<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Post extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('slug_category')->nullable();
            $table->text('content')->nullable();

            $table->text('path_image')->nullable();
            $table->text('link_image')->nullable();
            $table->text('link_video')->nullable();
            $table->text('link_doc')->nullable();
            $table->text('many_image')->nullable();

            $table->text('seo_title')->nullable();
            $table->text('seo_keyword')->nullable();
            $table->text('seo_description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
