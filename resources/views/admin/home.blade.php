@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <form action="{{route('admin.agc.process')}}" id="form-search" class="horizontal-form form-bordered" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Date</label>
                                        <input type="text" id="date" value="{{old('date')}}" name="date" class="form-control" placeholder="yyyy-mm-dd">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">slug_category</label>
                                        <input type="text" id="date" value="{{old('slug_category')}}" name="slug_category" class="form-control" placeholder="slug_category">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">File CSV</label>
                                        <input type="file" id="filecsv"  name="filecsv" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions right">
                            <button type="submit" class="btn btn-primary"><strong> Upload</strong></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
