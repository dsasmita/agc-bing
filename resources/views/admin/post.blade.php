@extends('admin.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Post</div>

                <div class="panel-body">
                    <div class="bs-example" data-example-id="hoverable-table">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th><center>Img</center></th>
                                    <th><center>Title</center></th>
                                    <th><center>Action</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($posts)
                                    @foreach($posts as $li)
                                    <tr>
                                        <td>
                                            @php
                                            $filename = pathinfo($li->path_image);
                                            @endphp
                                            <center>
                                                <a href="{{url($li->path_image)}}" target="_blank">
                                                    <img alt="{{$li->title}}" style="width: 100px" src="{{url(@$filename['dirname'].'/'.$filename['filename'].'_square.'.$filename['extension'])}}">
                                                </a>
                                            </center>
                                        </td>
                                        <td>
                                            {{$li->title}}<br>
                                            <small>
                                                {{$li->date}} - {{$li->slug_category}}<br> {{ $li->updated_at }}<br>
                                                {{$li->source}} - {{$li->grower}} - related: {{ count(explode(',', $li->seo_keyword)) }}
                                            </small>
                                        </td>
                                        <td>
                                            <center>
                                                <a target="_blank" href="{{ route('admin.post.grower',$li->id) }}" class="btn btn-sm btn-info">grower</a>
                                                <a href="{{ route('admin.post.delete',$li->id) }}" class="btn btn-sm btn-warning">delete</a>
                                            </center>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                <tr>
                                    <td colspan="4"><center>-empty-</center></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        <span class="pull-right">{{$posts->count()}} from {{$posts->total()}}</span>
                        <center>{{ $posts->appends($filter)->links() }}</center>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
