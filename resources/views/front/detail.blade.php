@extends('front.layouts.app')

@section('content')
<div class="number">
	<h3><a href="{{ route('front.detail',$post->slug) }}">{{ $post->title }}</a></h3>
	<small><em>Post On {{date('F j,Y', strtotime($post->date))}} In <a href="#">{{$post->slug_category}}</a></em></small>

	@if(env('APP_ENV') == 'production' && Auth::guest())
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Foomka.com -->
	<ins class="adsbygoogle"
	     style="display:block"
	     data-ad-client="ca-pub-0124899135832986"
	     data-ad-slot="5987595951"
	     data-ad-format="auto"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	<hr>
	@endif

	<div class="span_8" style="margin-top: 10px">
	    <div class="row_8">
	        <div class="sap_tabs">
	            <center>
	            	@php
                    $filename = pathinfo($post->path_image);
                    @endphp
	                <img src="{{url(@$filename['dirname'].'/'.$filename['filename'].'_700px.'.@$filename['extension'])}}" class="img-responsive" alt=""> 
	            </center>
	        </div>
	    </div>
	</div>

	@if(env('APP_ENV') == 'production' && Auth::guest())
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Foomka.com -->
	<ins class="adsbygoogle"
	     style="display:block"
	     data-ad-client="ca-pub-0124899135832986"
	     data-ad-slot="5987595951"
	     data-ad-format="auto"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	<hr>
	@endif

	<p>{{$post->content}}</p>

	@if(env('APP_ENV') == 'production' && Auth::guest())
	<hr>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Foomka.com -->
	<ins class="adsbygoogle"
	     style="display:block"
	     data-ad-client="ca-pub-0124899135832986"
	     data-ad-slot="5987595951"
	     data-ad-format="auto"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	@endif
	
</div>
<div class="cinema">
	<h1>Related Post</h1>
	<hr style="border: 1">
	<div class="cinema-top">
		@foreach($lists as $li)
		@php
        $filename = pathinfo($li->path_image);
        @endphp
		<div class="col-md-4 top-cinema">
			<a href="{{ route('front.detail',$li->slug) }}" title="{{ $li->title }}"><img class="img-responsive" src="{{url(@$filename['dirname'].'/'.$filename['filename'].'_square.'.@$filename['extension'])}}" alt="{{$li->title}}"></a>
			<div class="caption">
				<a href="{{ route('front.detail',$li->slug) }}" title="{{ $li->title }}"><p>{{str_limit($li->title,15)}}</p></s>
			</div>
		</div>
		@endforeach
		<div class="clearfix"></div>
	</div>
</div>
@endsection
