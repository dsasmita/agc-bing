<!DOCTYPE html>
<html>
    <head>
        <title>{{@$title}}</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="{{@$seo['keywords']}}" />
        <meta name="robots" content="index,follow">
        <meta name="googlebot" content="index,follow" />

        <meta name="author" content="{{ env('SEO_SITE_NAME') }}">
        <meta property='og:site_name' content="{{ env('SEO_SITE_NAME') }}" />
        <meta property="og:title" content="{{ @$seo['title'] }}" />
        <meta property="og:description" content="{{ @$seo['description'] }}" />
        <meta property="og:url" content="{{ @$seo['url'] }}" />
        <meta property="og:image" content="{{ @$seo['image'] }}" />

        <!-- CSS -->
        <link href="{{url('assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{url('assets/css/nav.css')}}" rel="stylesheet" type="text/css" media="all"/>
    </head>
    <body>
        <div class="container">
            <div class="main-top">
                <div class="main">
                    <div class="header">
                        <div class="header-top">
                            <div class="header-in">
                                <div class="logo">
                                    <a href="{{route('front.home')}}"><img src="{{url('assets/images/logo.png')}}" style="width: 190px" alt="{{ env('SEO_SITE_NAME') }}" title="{{ env('SEO_SITE_NAME') }}" ></a>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="header-bottom">
                            <div class="navigation">
                                <div>
                                    <label class="mobile_menu" for="mobile_menu">
                                    <span>Menu</span>
                                    </label>
                                    <input id="mobile_menu" type="checkbox">
                                    <ul class="nav" style="background-color: #999">
                                        <li><a class="color1" href="{{route('front.home')}}">Home</a></li>
                                        @php
                                        $categories = explode('|',env('SITE_SUBMENU'));
                                        @endphp
                                        @foreach($categories as $key => $cat)
                                            <li><a href="{{ route('front.category',$cat) }}" class="color{{$key+2}}">{{ $cat }}</a></li>
                                        @endforeach
                                        {{--
                                        <li><a href="archives.html" class="color5">Sitemap</a></li>
                                        <li><a href="contact.html" class="color6">Contact us</a></li>
                                        --}}
                                        <div class="clearfix"></div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <div class="col-md-8 content-top">
                            @yield('content')
                        </div>
                        <div class="col-md-4 content-bottom">
                            <div class="might">
                                <h4>You Might Also Like</h4>
                                @foreach(\App\Models\post::inRandomOrder()->limit(15)->get() as $li)
                                @php
                                $filename = pathinfo($li->path_image);
                                @endphp
                                <div class="might-grid">
                                    <div class="grid-might">
                                        <a href="{{ route('front.detail',$li->slug) }}"><img style="width: 85px" src="{{url(@$filename['dirname'].'/'.$filename['filename'].'_square.'.$filename['extension'])}}" class="img-responsive" alt="{{ $li->title }}"> </a>
                                    </div>
                                    <div class="might-top">
                                        <p>{{str_limit($li->content,60)}}</p>
                                        <a href="{{ route('front.detail',$li->slug) }}">Detail <i> </i></a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                @endforeach
                            </div>
                            @if(@$post && @$post->seo_keyword !== '')
                                <div class="grid-top-in">
                                    <h4>Keyword</h4>
                                    <p>
                                        @php
                                        $words = explode(',', $post->seo_keyword);
                                        @endphp
                                        @foreach($words as $li)
                                            <a href="#" class="bun">{{$li}}</a>
                                        @endforeach
                                    </p>   
                                </div>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <i class="line"> </i>
            </div>
            <p class="footer-class">Copyright &copy; <a href="{{ route('front.home') }}">{{ env('SEO_SITE_NAME') }}</a> Template by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
            
            <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
        </div>
        <script src="{{url('assets/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{url('assets/js/move-top.js')}}"></script>
        <script type="text/javascript" src="{{url('assets/js/easing.js')}}"></script>
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <script src="{{url('assets/js/main.js')}}"></script>
        <script src="{{url('assets/js/easyResponsiveTabs.js')}}" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#horizontalTab,#horizontalTab1,#horizontalTab2').easyResponsiveTabs({
                    type: 'default',
                    width: 'auto',
                    fit: true
                });
            });
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $(".scroll").click(function(event){     
                    event.preventDefault();
                    $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $().UItoTop({ easingType: 'easeOutQuart' });
            });
        </script>
        @if(env('APP_ENV') == 'production' && Auth::guest())
            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', '{{ env("G_ANALYTICS") }}', 'auto');
              ga('send', 'pageview');

            </script>
        @endif
    </body>
</html>