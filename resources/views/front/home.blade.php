@extends('front.layouts.app')

@section('content')
	@if(env('APP_ENV') == 'production' && Auth::guest())
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Foomka.com -->
	<ins class="adsbygoogle"
	     style="display:block"
	     data-ad-client="ca-pub-0124899135832986"
	     data-ad-slot="5987595951"
	     data-ad-format="auto"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	<hr>
	@endif
	
	@if($lists->count() > 0)
		@foreach($lists as $li)
			@php
		    $filename = pathinfo($li->path_image);
		    @endphp
			<div class="number">
				<h3><a href="{{ route('front.detail',$li->slug) }}">{{$li->title}}</a></h3>
				<small><em>Post On {{date('F j,Y', strtotime($li->date))}} In <a href="{{ route('front.category',$li->slug_category) }}">{{$li->slug_category}}</a></em></small>
				<a href="{{ route('front.detail',$li->slug) }}">
					<div class="span_8" style="margin-top: 10px">
					    <div class="row_8">
					        <div class="sap_tabs">
					            <center>
					                <img alt="{{ $li->title }}" title="{{ $li->title }}" src="{{url(@$filename['dirname'].'/'.$filename['filename'].'_square.'.$filename['extension'])}}" class="img-responsive"> 
					            	<small><em>{{ $li->title }}</em></small>
					            </center>
					        </div>
					    </div>
					</div>
				</a>
				<p>{{str_limit($li->content,350)}}</p>
				<div class="movie">
				    <a href="{{ route('front.detail',$li->slug) }}" class="more">Read More </a>
				    <div class="clearfix"> </div>
				</div>
			</div>
		@endforeach
		<div class="older-top">
			<a href="{{$lists->previousPageUrl()}}" class="older"><i> </i>newer posts</a>
			<a href="{{$lists->nextPageUrl()}}" class="older top-old">older posts<i > </i></a>
			<div class="clearfix"> </div>
		</div>
	@else
		<center><h4>Empty post</h4></center>
		<h3>YOU MIGHT ALSO LIKE</h3>
		<hr>
	    @foreach(\App\Models\post::inRandomOrder()->limit(24)->get() as $li)
		    @php
		    $filename = pathinfo($li->path_image);
		    @endphp
			<div class="col-md-4 top-cinema">
				<a href="{{ route('front.detail',$li->slug) }}"><img class="img-responsive" src="{{url(@$filename['dirname'].'/'.$filename['filename'].'_square.'.$filename['extension'])}}" alt="{{$li->title}}"></a>
				<div class="caption">
					<a href="{{ route('front.detail',$li->slug) }}" title="{{ $li->title }}"><p>{{str_limit($li->title,15)}}</p></s>
				</div>
			</div>
	    @endforeach
	@endif

	@if(env('APP_ENV') == 'production' && Auth::guest())
	<hr>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Foomka.com -->
	<ins class="adsbygoogle"
	     style="display:block"
	     data-ad-client="ca-pub-0124899135832986"
	     data-ad-slot="5987595951"
	     data-ad-format="auto"></ins>
	<script>
	(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
	@endif
@endsection